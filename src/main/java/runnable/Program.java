package main.java.runnable;

import main.java.services.*;

public class Program {

    public static void main(String[] args) {
        int a = 5;
        int b = 10;
        int c = 15;
        int d = 20;
        int e = 0;
        int op;

        Operator operator = new Operator(new Adder());
        try {
            op = operator.operation(a, b);
            System.out.println(a + " + " + b + " = " + op);
        } catch (Exception exception) {
            System.out.println("Error in operation: " + exception);
        }

        operator.setOperationType(new Substractor());
        try {
            op = operator.operation(d, c);
            System.out.println(d + " - " + c + " = " + op);
        } catch (Exception exception) {
            System.out.println("Error in operation: " + exception);
        }

        operator.setOperationType(new Multiplier());
        try {
            op = operator.operation(a, d);
            System.out.println(a + " * " + d + " = " + op);
        } catch (Exception exception) {
            System.out.println("Error in operation: " + exception);
        }

        operator.setOperationType(new Divider());
        //testing for error
        try {
            op = operator.operation(c, e);
            System.out.println(c + " / " + e + " = " + op);
        } catch (Exception exception) {
            System.out.println("Error in operation: " + exception);
        }

        try {
            op = operator.operation(d, b);
            System.out.println(d + " / " + b + " = " + op);
        } catch (Exception exception) {
            System.out.println("Error in operation: " + exception);
        }

        operator.setOperationType(new Exponentiator());
        try {
            op = operator.operation(b, a);
            System.out.println(b + " ^ " + a + " = " + op);
        } catch (Exception exception) {
            System.out.println("Error in operation: " + exception);
        }

    }
}
