package main.java.services;

import main.java.services.OperationInterface;

public class Divider implements OperationInterface {
    @Override
    public int operation(int a, int b) {
        try {
            float x = a/b;
            return (int)x;
        }
        catch (Exception e) {
            //possible divide by zero
            throw e;
        }

    }
}
