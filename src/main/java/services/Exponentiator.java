package main.java.services;

import java.lang.Math;
import main.java.services.OperationInterface;

public class Exponentiator implements OperationInterface {
    @Override
    public int operation(int a, int b) {
        try {
            return (int) Math.pow(a, b);
        }
        catch (Exception e) {throw e;}
    }
}
