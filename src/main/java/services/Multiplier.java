package main.java.services;

import main.java.services.OperationInterface;

public class Multiplier implements OperationInterface {

    @Override
    public int operation(int a, int b) {
        return a*b;
    }
}
