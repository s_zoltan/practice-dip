package main.java.services;

public interface OperationInterface {
    int operation(int a, int b) throws Exception;

}
