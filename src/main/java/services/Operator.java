package main.java.services;

import main.java.services.OperationInterface;

public class Operator {
    OperationInterface op;

    public Operator(OperationInterface op) {this.op = op;}

    public void setOperationType(OperationInterface op) {this.op = op;}

    public int operation (int a, int b) throws Exception {
        try {  return op.operation(a, b); }
        catch (Exception e) {throw e;}
    }

}
